﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotPad.baseStream
{
    class SecuentialStream
    {
        private string filepath;

        public SecuentialStream(string filepath)
        {
            this.filepath = filepath;
        }

        public void writeText(string text)
        {
            try
            {
                using(StreamWriter sw = new StreamWriter(filepath))
                {
                    sw.Write(text);
                }
            }
            catch (IOException){ }
        }

        public string readText()
        {
            string text = "";
            try
            {
                using(StreamReader sr = new StreamReader(filepath))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        text += line;
                    }
                }
            }
            catch (IOException) { }

            return text;
        }
    }
}
