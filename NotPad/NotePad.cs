﻿using NotPad.baseStream;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotPad
{
    public partial class NotePad : Form
    {
        public NotePad()
        {
            InitializeComponent();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pad pd = new Pad();
            pd.MdiParent = this;
            pd.Show();

        }

        private void quitNotPadToolStripMenuItem_Click(object sender, EventArgs e)
        {


        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK){
                

            }


        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int count = this.MdiChildren.Length;
            if(count == 0)
            {
                return;
            }
            DialogResult result = saveFileDialog1.ShowDialog();

            if(result == DialogResult)
            {
                string filepath = saveFileDialog1.FileName;
                SecuentialStream ss = new SecuentialStream(filepath);
                Form activeChild = this.ActiveMdiChild;
                TextBox txtarea = (TextBox)activeChild.Controls[0];
                ss.writeText(txtarea.Text);
            }

        }
    }
}
