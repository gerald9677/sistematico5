﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ReporteFactura
    {
        private string codFactura;
        private DateTime fecha;
        private double subtotal;
        private double iva;
        private double total;
        private int cantidad;
        private double precio;
        private string sku;
        private string nombreProducto;
        private string nombreEmpleado;
        private string apellidoEmpleado;

        public string CodFactura
        {
            get
            {
                return codFactura;
            }

            set
            {
                codFactura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }

        public string NmbreProducto
        {
            get
            {
                return nombreProducto;
            }

            set
            {
                nombreProducto = value;
            }
        }

        public string NombreEmpleado
        {
            get
            {
                return nombreEmpleado;
            }

            set
            {
                nombreEmpleado = value;
            }
        }

        public string ApellidoEmpleado
        {
            get
            {
                return apellidoEmpleado;
            }

            set
            {
                apellidoEmpleado = value;
            }
        }

        public ReporteFactura(string codFactura, DateTime fecha, double subtotal, double iva, double total, int cantidad, double precio, string sku, string nmbreProducto, string nombreEmpleado, string apellidoEmpleado)
        {
            this.CodFactura = codFactura;
            this.Fecha = fecha;
            this.Subtotal = subtotal;
            this.Iva = iva;
            this.Total = total;
            this.Cantidad = cantidad;
            this.Precio = precio;
            this.Sku = sku;
            this.NmbreProducto = nmbreProducto;
            this.NombreEmpleado = nombreEmpleado;
            this.ApellidoEmpleado = apellidoEmpleado;
        }
    }
}
