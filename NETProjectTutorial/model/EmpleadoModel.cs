﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class EmpleadoModel
    {
        private static List<Empleado> empleados = new List<Empleado>();

        public static List<Empleado> GetListEmpleados()
        {
            return empleados;
        }

        public static void Populate()
        {
            //Empleado[] emps =
            //{
            //    new Empleado(1, "1234567-1", "001-110388-5588D", "Pepito", "Perez", "del arbolito 2c. abajo", "22458951", "85459562", Empleado.Sexo.MASCULINO, 25145.33),
            //    new Empleado(2, "1234567-2", "001-120899-5414R", "Ana", "Conda", "del arbolito 2c. abajo", "28457596", "57452505", Empleado.Sexo.FEMENINO, 30145.33),
            //    new Empleado(3, "1234567-3", "001-020685-2154V", "Armando", "Guerra", "del arbolito 2c. abajo", "29874510", "78545652", Empleado.Sexo.MASCULINO, 12145.33)
            //};

            empleados = JsonConvert.DeserializeObject<List<Empleado>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.empleados_data));
        }
    }
}
